module icache(
    output reg[`WordSize-1:0] insOut,
    output reg ready,
    output reg[`Addr-1:0] memAddr,
    output reg memEn,
    input wire memReady,
    input wire[(`WordSize*`WordsInBlock)-1:0] blockFromMemory,
    input wire en,
    input wire[`Addr-1:0] wordAddr,
    input wire reset,
    input wire clk
);

wire[`Index-1:0] line;
wire[`Offset-1:0] word;
wire[`Tag-1:0] tag;

reg[`WordSize-1:0] icachemem[0:`Blocks-1][0:`WordsInBlock-1];
reg[`Tag-1:0] tagReg[0:`Blocks-1];
reg valid[0:`Blocks-1];

wire hitMiss;

assign line = wordAddr[`LStrobe];
assign word = wordAddr[`WStrobe];
assign tag = wordAddr[`TStrobe];

assign hitMiss = ~|(tagReg[line] ^ tag) & valid[line];//hitMiss
//Cache controller declarations
reg state;//0-idle/hit 1-miss


//Reset- 0 - 1
genvar i;
generate
    for(i=0;i<`Blocks;i=i+1)begin
        always@(posedge clk)
            if(reset)begin
                valid[i]=1'b0;
            end
            
    end
endgenerate

always@(posedge clk)
    if(reset)begin
        state<=1'b0;
        memEn<=1'b0;
    end


//hit
// always@(posedge clk)
//     if(en & hitMiss & ~state[0] & ~state[1] & opSel)begin
//         dcachemem[line][word]<=data;
//         dirty[line]=1'b1;
//     end

// always@(negedge clk)
//     if(en & hitMiss & ~state[0] & ~state[1] & ~opSel)
//         dataOut<=dcachemem[line][word];

// always@(posedge clk)begin
//     if(state==2'b10)
//         if(dirty[line])begin
//             memAddr_str<=wordAddr;
//             memEn_str<=1'b0;
//             memReadWrite<=1'b1;
            
//         end


always@(posedge clk)
if(en)
    case(state)
        1'b0:
            if(hitMiss)begin
                insOut<=icachemem[line][word];
                ready<=1'b1;
            end
            else begin
                memAddr<={tag,wordAddr[line],`BlockMask};
                memEn<=1'b1;
                state<=2'b1;
                valid[line]<=1'b0;
                ready<=1'b0;
            end
        1'b1:
            if(memReady)begin
            memEn<=1'b0;
            {icachemem[line][15],icachemem[line][14],icachemem[line][13],icachemem[line][12],icachemem[line][11],
            icachemem[line][10],icachemem[line][9],icachemem[line][8],icachemem[line][7],icachemem[line][6],
            icachemem[line][5],icachemem[line][4],icachemem[line][3],icachemem[line][2],icachemem[line][1],
            icachemem[line][0]}=blockFromMemory;
            tagReg[line]=tag;
            valid[line]=1'b1;
            state<=1'b0;
            end
    endcase
else
    ready=1'b0;
endmodule
