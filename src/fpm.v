// `include "Parallel_Prefix_CLA.v"
// `include "wallace_multiplier.v"
// `include "incrementer.v"

module Fpm(output wire[15:0] out,input wire[15:0] ain,input wire[15:0] bin);
wire[7:0] sum_exp_int,sum_exp,sum_exp_norm,sum_exp_inc;
wire[4:0] ea,eb;
wire[9:0] ma,mb,manOut;
wire[31:0] mul_out;
wire signOut,exp_overflow,exp_underflow,inc_overflow;
wire[14:0] numOut;

assign ea = ain[14:10];
assign eb = bin[14:10];

assign ma = ain[9:0];
assign mb = bin[9:0];


assign signOut = ain[15] ^ bin[15];
Adder #(.wd(8),.itr(3)) add1(.overflow(),.sum(sum_exp_int),.a({3'b0,ea}),.b({3'b0,eb}),.cin(1'b0),.ctr(1'b0));
Adder #(.wd(8),.itr(3)) sub1(.overflow(exp_underflow),.sum(sum_exp_norm),.a(sum_exp_int),.b(8'b1111),.cin(1'b1),.ctr(1'b1));

assign exp_overflow = sum_exp_int[5] & ~exp_underflow;

Multiplier mul(.msb(mul_out[31:16]),.lsb(mul_out[15:0]),.a({5'b0,ea[0]|ea[1]|ea[2]|ea[3]|ea[4],ma}),.b({5'b0,eb[0]|eb[1]|eb[2]|eb[3]|eb[4],mb}));
//incr #(.wd(8)) inc(.out(sum_exp_inc),.in(sum_exp_norm));
Adder #(.wd(8),.itr(3)) inc(.overflow(inc_overflow),.sum(sum_exp_inc),.a(sum_exp_norm),.b(8'b1),.cin(1'b0),.ctr(1'b0));

assign sum_exp = (mul_out[21])?sum_exp_inc:sum_exp_norm;
`ifdef VON
    assign manOut = (mul_out[21])?{mul_out[20:12],|mul_out[11:0]}:{mul_out[19:11],|mul_out[10:0]};//von neumann truncation
`else
    `ifdef ROUND
        assign manOut = (mul_out[21])?mul_out[20:11] + mul_out[10]:mul_out[19:10]+mul_out[9];//roundoff
    `else
        assign manOut = (mul_out[21])?mul_out[20:11]:mul_out[19:10];//truncation
    `endif
`endif
wire expDiffIsZero,manDiffIsZero,manAIsZero,manBIsZero,manOutIsZero,inIsZero;
wire inIsDenorm,inIsInf,inIsNaN,zero,expAIsOne,expAIsZero,expBIsOne,expBIsZero;
wire aIsInf,aIsNaN,bIsInf,bIsNaN,expOutIsOne,expOutIsZero,outIsDenorm,outIsInf,overflow,naN,underflow;

assign numOut = { sum_exp[4:0] , manOut };
// Input exception signals
assign expAIsOne = &ain[14:10];
assign expBIsOne = &bin[14:10];
assign expAIsZero = ~|ain[14:10];
assign expBIsZero = ~|bin[14:10];
assign manAIsZero = ~|ain[9:0];
assign manBIsZero = ~|bin[9:0];
assign aIsNaN = expAIsOne & ~manAIsZero ;
assign bIsNaN = expBIsOne & ~manBIsZero ;
assign aIsInf = expAIsOne & manAIsZero ;
assign bIsInf = expBIsOne & manBIsZero ;
assign inIsNaN = aIsNaN | bIsNaN ;
assign inIsInf = aIsInf | bIsInf ;
assign inIsDenorm = expAIsZero | expBIsZero;
assign inIsZero = (expAIsZero & manAIsZero) | (expBIsZero & manBIsZero);
// Output exception signals
assign expOutIsOne = & sum_exp ;
assign expOutIsZero = ~|sum_exp ;
assign manOutIsZero = ~|manOut ;
assign outIsInf = expOutIsOne | inc_overflow | exp_overflow;
assign outIsDenorm = expOutIsZero | exp_underflow;
assign zero = (expOutIsZero & manOutIsZero);
// Final exception signals
assign overflow = ( inIsInf | outIsInf ) & ~zero & ~inIsZero ;
assign naN = inIsNaN | (inIsInf & inIsZero);
assign underflow = inIsDenorm | outIsDenorm | zero | inIsZero ;
assign out = (naN)?16'h7fff:((overflow)?{signOut,15'b111110000000000}:((underflow)?16'b0:{signOut,numOut}));

// always@(manOut)
//     $display("sum_exp_int:%b sum_exp_norm:%b sum_exp:%b mul_out:%h man_out:%b numOut=%b",sum_exp_int,sum_exp_norm,sum_exp,mul_out,manOut,numOut);

endmodule
