//`include "Parallel_Prefix_CLA.v"
//`include "imem.v"
module ifu #(parameter width=16,parameter addr_width = 16,parameter idepth=1024,parameter itr=4)(output wire[width-1:0] instructionReg,input wire clk,input wire rst,input wire en);
reg[addr_width-1:0] pc;
wire[addr_width-1:0] inc_sum;
Adder #(.wd(addr_width),.itr($clog2(addr_width))) inc(.overflow(),.sum(inc_sum),.a(pc),.b(16'b1),.cin(1'b0),.ctr(1'b0));
InstructionMemory #(.width(width),.addr_line(addr_width),.idepth(idepth)) im(.data(instructionReg),.clk(clk),.address(pc),.chip_en(en));
always@(posedge rst)
    pc<=0;
always@(posedge clk)
    pc<=(en && ~rst) ? inc_sum : 0;
endmodule