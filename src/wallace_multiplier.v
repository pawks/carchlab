module csa(output wire[31:0] c,output wire[31:0] s,input wire[31:0] a,input wire[31:0] b,input wire[31:0] x);
    assign s = a ^ b ^ x;
    assign c = {((a&b)+(x&(a^b))),1'b0};
endmodule


module Multiplier( output wire[15:0]  msb, output wire[15:0] lsb,input wire[15:0] a,input wire[15:0] b);
    wire [31:0] m[15:0];
    wire [31:0] s[13:0];
    wire [31:0] c[13:0];
    
    //Partial Products Generation
    assign m[0]= b[0]?{16'b0,a}:0;   
    assign m[1]= b[1]?{15'b0,a,1'b0}:0;   
    assign m[2]= b[2]?{14'b0,a,2'b0}:0;   
    assign m[3]= b[3]?{13'b0,a,3'b0}:0;   
    assign m[4]= b[4]?{12'b0,a,4'b0}:0;   
    assign m[5]= b[5]?{11'b0,a,5'b0}:0;   
    assign m[6]= b[6]?{10'b0,a,6'b0}:0;   
    assign m[7]= b[7]?{9'b0,a,7'b0}:0;
    assign m[8]= b[8]?{8'b0,a,8'b0}:0;
    assign m[9]= b[9]?{7'b0,a,9'b0}:0;
    assign m[10]= b[10]?{6'b0,a,10'b0}:0;
    assign m[11]= b[11]?{5'b0,a,11'b0}:0;
    assign m[12]= b[12]?{4'b0,a,12'b0}:0;
    assign m[13]= b[13]?{3'b0,a,13'b0}:0;
    assign m[14]= b[14]?{2'b0,a,14'b0}:0;
    assign m[15]= b[15]?{1'b0,a,15'b0}:0;

    //CSA stages
    csa c1(c[0],s[0],m[0],m[1],m[2]);
    csa c2(c[1],s[1],m[3],m[4],m[5]);
    csa c3(c[2],s[2],m[6],m[7],m[8]);
    csa c4(c[3],s[3],m[9],m[10],m[11]);
    csa c5(c[4],s[4],m[13],m[14],m[12]);
    csa c6(c[5],s[5],m[15],c[0],s[0]);
    csa c7(c[6],s[6],s[1],c[1],s[2]);
    csa c8(c[7],s[7],c[2],c[3],s[3]);
    csa c9(c[8],s[8],s[4],c[4],s[5]);
    csa c10(c[9],s[9],c[5],s[6],c[6]);
    csa c11(c[10],s[10],s[7],c[7],s[8]);
    csa c12(c[11],s[11],c[8],s[9],c[9]);
    csa c13(c[12],s[12],s[10],s[11],c[10]);
    csa c14(c[13],s[13],c[11],s[12],c[12]);
    wire overflow;
    //ADDER
    Adder #(.wd(32),.itr(5)) add(overflow,{msb,lsb},s[13],c[13],1'b0,1'b0);
endmodule