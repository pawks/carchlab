module logicXOR(output wire[15:0] rdst, input wire[15:0] rs1,input wire[15:0] rs2);
assign rdst = rs1 ^ rs2;
endmodule

module logicNAND(output wire[15:0] rdst,input wire[15:0] rs1,input wire[15:0] rs2);
assign rdst = rs1 ~& rs2;
endmodule

module logicNEG(output wire[15:0] rdst,input wire[15:0] rs);
assign rdst = ~rs;
endmodule

module shLeft(output reg[15:0] out,input wire[15:0] a,input wire[3:0] b);
always@(a)
    out = a<<b;
endmodule

module shRight(output reg[15:0] out,input wire[15:0] a,input wire[3:0] b);
always@(a)
    out = a>>b;
endmodule
