// module register (out, in, wen, rst, clk);
//   parameter n = 1;
//   output [n-1:0] out;
//   input [n-1:0] in;
//   input wen;
//   input rst;
//   input clk;

//   reg [n-1:0] out;

//   always @(posedge clk) 
//     begin 
//       if (rst) 
//         out = 0; 
//       else if (wen) 
//         out = in; 
//     end 
// endmodule
module reg_file #(parameter wd=16,parameter no_reg=16,parameter addr=4)(
    input wire clk,
    input wire rst,
    input wire ren1,
    input wire ren2,
    input wire wen,
    input wire[addr-1:0] r1_addr,
    input wire[addr-1:0] r2_addr,
    input wire[addr-1:0] wr_addr,
    input wire[wd-1:0] wr_data,
    output wire[wd-1:0] r1_dat_out,
    output wire[wd-1:0] r2_dat_out
);
reg[wd-1:0] r1_dat;
reg[wd-1:0] r2_dat;
reg[wd-1:0] regs[no_reg-1:0];
wire[wd-1:0] r1_dat_int;
wire[wd-1:0] r2_dat_int;
assign r1_dat_int = regs[r1_addr];
assign r2_dat_int = regs[r2_addr];
assign r1_dat_out = (ren1)? r1_dat : 16'bx;
assign r2_dat_out = (ren2)? r2_dat : 16'bx;
genvar i;
generate
    for(i=0;i<no_reg;i=i+1)begin
    always@(posedge rst)
        regs[i]<=0;
    end
endgenerate
always@(posedge clk)
    if(wen&~rst)
        regs[wr_addr]<=wr_data;
always@(negedge clk)
    if(~rst&ren1)
        r1_dat<=r1_dat_int;
always@(negedge clk)
    if(~rst&ren2)
        r2_dat<=r2_dat_int;

endmodule