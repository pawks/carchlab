`ifndef MEMDIRECTIVES
    `define MEMDIRECTIVES
    `define WordsInBlock 16
    `define Addr 16
    `define MIndex `Addr-`Offset
    `define WordSize 16
    `define Blocks 64
    `define Index 6
    `define Offset 4
    `define Rows 64
    `define Columns 64
    `define RStrobe ((`MIndex)/2)+`Offset-1:`Offset
    `define CStrobe `Addr-1:(`MIndex)/2+`Offset
    `define Tag `Addr-`Index-`Offset
    `define LStrobe `Offset+`Index-1:`Offset
    `define WStrobe `Offset-1:0
    `define TStrobe `Addr-1:`Offset+`Index
    `define BlockMask 4'b0000
`endif