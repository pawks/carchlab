module Generator #(parameter wd=16) (output wire[wd:0] g,output wire[wd:0] p,input wire cin,input wire[wd-1:0] a, input wire[wd-1:0] b);
    assign g = { a & b , cin };
    assign p = { a ^ b , 1'b0 };
endmodule

module Kgp_eval(output wire g,output wire p,input wire g1,input wire p1,input wire g2,input wire p2);
    wire con1;
    and a1(con1,p1,g2);
    or o1(g,con1,g1);
    and a2(p,p1,p2);
endmodule

module Kg_eval(output wire g,input wire g1,input wire p1,input wire g2);
    wire con1;
    and a1(con1,p1,g2);
    or o1(g,con1,g1);
endmodule

module Int_state #(parameter wd=16,parameter stage=1)(output wire[wd-1:0] gout,output wire[wd-1:0] pout, input wire[wd-1:0] gin, input wire[wd-1:0] pin);
    genvar i;
    generate
        for(i=0;i+2**stage<wd;i=i+1)
            Kgp_eval unit(.g(gout[i+2**stage]), .p(pout[i+2**stage]), .g1(gin[i+2**stage]), .p1(pin[i+2**stage]), .g2(gin[i]), .p2(pin[i]));
    endgenerate
    assign gout[2**stage-1:0] = gin[2**stage-1:0];
    assign pout[2**stage-1:0] = pin[2**stage-1:0];
endmodule

module Parallel_Prefix_circuit #(parameter wd=16,parameter itr=4) (output wire[wd-1:0] out,input wire[wd-1:0] gin,input wire[wd-1:0] pin);
    wire[wd-1:0] gcon[itr-1:0], pcon[itr-1:0], dum;
    Int_state #(.wd(wd), .stage(0)) int0(.gout(gcon[0]), .pout(pcon[0]), .gin(gin), .pin(pin));
    genvar i;
    generate
        for(i=1;i<itr-1;i=i+1)
            Int_state #(.wd(wd), .stage(i)) inti(.gout(gcon[i]), .pout(pcon[i]), .gin(gcon[i-1]), .pin(pcon[i-1]));
    endgenerate
    Int_state #(.wd(wd), .stage(itr-1)) intf(.gout(out), .pout(), .gin(gcon[itr-2]), .pin(pcon[itr-2]));
endmodule

module Adder #(parameter wd=16,parameter itr=4) (output wire overflow,output wire[wd-1:0] sum,input wire[wd-1:0] a,input wire[wd-1:0] b,input wire cin,input wire ctr);
    wire[wd:0] pint,gint;
    wire[wd-1:0] prefix_out,bint;
    wire overflow_int;
    genvar i;
    generate
        for(i=0;i<wd;i=i+1)
            xor neg(bint[i],b[i],ctr);
    endgenerate
    Generator #(.wd(wd)) pre_comp(.g(gint),.p(pint),.cin(cin),.a(a),.b(bint));
    Parallel_Prefix_circuit #(.wd(wd),.itr(itr)) prefix_eval(.out(prefix_out),.gin(gint[wd-1:0]),.pin(pint[wd-1:0]));
    Kg_eval overflow_eval(.g(overflow_int),.g1(gint[wd]),.p1(pint[wd]),.g2(prefix_out[wd-1]));
    assign sum= {pint[wd:1]} ^ prefix_out;
    assign overflow = overflow_int ^ ctr;
endmodule