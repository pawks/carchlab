module memory(
    output reg[(`WordSize*`WordsInBlock)-1:0] iout,
    output reg[(`WordSize*`WordsInBlock)-1:0] dout,
    output reg iready,
    output reg dready,
    input wire[`Addr-1:0] iaddr,
    input wire[`Addr-1:0] daddr,
    input wire[(`WordSize*`WordsInBlock)-1:0] din,
    input wire ien,
    input wire den,
    input wire dop,
    input wire clk
);
reg[(`WordSize*`WordsInBlock)-1:0] mem[0:`Columns-1][0:`Rows-1];
wire[`Addr-1-(`MIndex)/2+`Offset:0] inc,dc;
wire[((`MIndex)/2)-1:0] inr,dr;
wire[255:0] ol;

assign ol=mem[0][0];
assign inc = iaddr[`CStrobe];
assign dc = daddr[`CStrobe];
assign inr = iaddr[`RStrobe];
assign dr = daddr[`RStrobe];

`ifdef INITMEM
initial begin
    $readmemb("mem.list",mem);
end
`endif

always @(negedge clk)
if(ien)begin
  iout <= mem[inc][inr];
  iready <= 1'b1;
end
else begin
    iready <= 1'b0;
end

always @(negedge clk)
if(den & !dop) begin
    dout <= mem[dc][dr];
    dready <= 1'b1;
end

always @(posedge clk)
if(den & dop)begin
    mem[dc][dr] <= din;
    dready <= 1'b1;
end

always @(negedge clk)
if(!den)
    dready <= 1'b0;

// always @(posedge clk)
//     $display("m0:%b",ol[15:0]);
endmodule
