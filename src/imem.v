module InstructionMemory #(parameter width=16,parameter addr_line=16,parameter idepth=1024) (output reg[width-1:0] data,input wire clk,input wire[addr_line-1:0] address,input wire chip_en);
reg[width-1:0] imem[idepth-1:0];
initial begin
  $readmemb("imem.list",imem);
end
always@(posedge clk)
    data = (chip_en) ? imem[address] : 8'b0;
endmodule
