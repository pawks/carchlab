`include "dff.v"
`include "clock.v"

module test();
wire clk;
wire[15:0] out;
timer t(clk);
reg[15:0] in;
reg rst;
pipeReg p1(out,in,clk,rst);
initial begin
  rst<=0;
  #4
  rst<=1;
  in<=16'hff00;
  #4
  $finish;
end
always @(*)
    $display("%t: d:%h rst:%b q:%h",$time,in,rst,out);
endmodule