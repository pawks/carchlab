primitive Dff (q, d, clk, rst);    // sequential UDP
  output q;
  input clk, rst, d;
  reg q;
  initial q = 0;
  table
    // d  clk  rst : old q : q
       ?   ?    0  :   ?   : 0;
       0   R    1  :   ?   : 0;
       1   R    1  :   ?   : 1;
       ?   N    1  :   ?   : -;
       *   ?    1  :   ?   : -;
       ?   ?   (0?):   ?   : -;
  endtable
endprimitive

module pipeReg #(parameter wd=16)(output wire[wd-1:0] out,input wire[wd-1:0] in,input wire clk,input wire rst);

genvar i;
generate
  for(i=0;i<wd;i=i+1)
    Dff d1(out[i],in[i],clk,rst);
endgenerate

endmodule