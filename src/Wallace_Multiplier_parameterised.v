module full_adder(output wire s,output wire cout,input wire a,input wire b,input wire c);
assign s = a ^ b ^ c;
assign cout = a&b | b&c | c&a;
endmodule

module half_adder(output wire s,output wire cout,input wire a,input wire b);
assign s = a ^ b;
assign cout = a & b;
endmodule

module  CSA #(parameter wd1=16,parameter wd2=16,parameter wd3=16) (
    output wire[(wd2==wd3)?wd3:wd3-1:0] v,
    output wire[wd3-1:0] u,
    input wire[wd1-1:0] a,
    input wire[wd2-1:0] b,
    input wire[wd3-1:0] c);
    assign v[0]=0;
    genvar i;
    generate
        for(i=0;i<wd1;i=i+1)
            full_adder fa(.s(u[i]),.cout(v[i+1]),.a(a[i]),.b(b[i]),.c(c[i]));
        for(i=wd1;i<wd2;i=i+1)
            half_adder ha(.s(u[i]),.cout(v[i+1]),.a(b[i]),.b(c[i]));
        for(i=wd2;i<wd3;i=i+1)
            assign u[i]=c[i];
        for(i=wd2+1;i<=((wd2==wd3)?wd3:wd3-1);i=i+1)
            assign v[i]=0;
        
    endgenerate
endmodule

module Partial_Products_Generator #(parameter wd=16) (output wire[wd-1:0] pp[wd-1:0],input wire[wd-1:0] a,input wire[wd-1:0] b)

module Wallace_Multiplier #(parameter wd=16) (output wire[wd-1:0] msb,output wire[wd-1:0] lsb,input wire[wd-1:0] a,input wire[wd-1:0] b);
    
    genvar i;


endmodule

// module test();
//     reg[4:0] a,b;
//     reg[5:0] c;
//     wire[5:0] v;
//     wire[5:0] u;
//     CSA #(5,5,6) ca (v,u,a,b,c);
//     initial begin
//         a= 5'b11111;
//         b= 5'b11111;
//         c= 6'b111111;
//         #5
//         $finish;
//     end
//     always @*
//         $display("%b %b %b\t u:%b v:%b",a,b,c,u,v);
// endmodule