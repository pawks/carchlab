//`include "Parallel_Prefix_CLA.v"
module Find_Big_diff(output wire[15:0] a,output wire[15:0] b,output wire[4:0] diff,
                    input wire[15:0] ain,input wire[15:0] bin);
wire[7:0] sum1,sum2;
wire[15:0] summnp;
Adder #(.wd(8),.itr(3)) add1(.overflow(),.sum(sum1),.a({3'b0,ain[14:10]}),.b({3'b0,bin[14:10]}),.cin(1'b1),.ctr(1'b1));
Adder #(.wd(8),.itr(3)) add2(.overflow(),.sum(sum2),.a({3'b0,bin[14:10]}),.b({3'b0,ain[14:10]}),.cin(1'b1),.ctr(1'b1));
wire isdiffzero = ~|sum1;
Adder #(.wd(16),.itr(4)) addmnp(.overflow(),.sum(summnp),.a({6'b0,ain[9:0]}),.b({6'b0,bin[9:0]}),.cin(1'b1),.ctr(1'b1));
assign a = (isdiffzero)?((summnp[10])?bin:ain):((sum1[5])?bin:ain);
assign b = (isdiffzero)?((summnp[10])?ain:bin):((sum1[5])?ain:bin);
assign diff = (sum1[5])?sum2[4:0]:sum1[4:0];
endmodule

module Lead_zero(output wire[3:0] out,output wire azero,input wire[9:0] in);
//reg[9:0] in;
wire[3:0] diff;
wire val1;
wire[3:0] val4;
wire[1:0] val2;
assign azero = ~|in;
assign diff[3] = ~|in[9:2];
assign diff[2] = (~|in[9:6])& (~diff[3]);
assign val4 = diff[2] ? in[5:2] : in[9:6]; 
assign diff[1] = ~|val4[3:2] & ~diff[3] | (in[0] & diff[3] & ~in[1]);
assign val2 = diff[1] ? val4[1:0] : val4[3:2]; 
assign val1 = ~val2[1] & ~diff[3];
assign diff[0] = (diff[3] & in[1]) | val1 ;
assign out = diff + 1'b1;
//initial begin
//in = 10'b0000000001;
//#5
//in = 10'b0000000011;
//#5
//in = 10'b0000000111;
//#5
//in = 10'b0000001111;
//#5
//in = 10'b0000011111;
//#5
//in = 10'b0000111111;
//#5
//in = 10'b0001111111;
//#5
//in = 10'b0011111111;
//#5
//in = 10'b0111111111;
//#5
//in = 10'b1111111111;
//#5
//in = 10'b0000000000;
//$finish;
//end
//always @(*)
//    $monitor("diff:%b input:%b azero:%b",diff,in,azero);
endmodule

module B_Shifter_R(output wire[10:0] out,input wire[10:0] in,input wire[4:0] shift);
wire[10:0] stage1,stage2,stage3,stage4;
assign stage1 = (shift[0]) ? {1'b0,in[10:1]} : in;
assign stage2 = (shift[1]) ? {2'b0,stage1[10:2]} : stage1;
assign stage3 = (shift[2]) ? {4'b0,stage2[10:4]} : stage2;
assign stage4 = (shift[3]) ? {8'b0,stage3[10:8]} : stage3;
assign out = (shift[4]) ? {11'b0} : stage4;
endmodule

/*
module output_eval(output wire[15:0] out,input wire underflow,input wire overflow,input wire naN,input wire[14:0] numOut,input wire signOut);
always@(*)
begin
if(naN)
    out <= 16'h7fff;
else if(overflow)
    out <= {signOut,15'b111110000000000};
else if(underflow)
    out <= 16'b0;
else
    out <= {signOut ,numOut};
end 
endmodule
*/

module B_Shifter_L(output wire[11:0] out,input wire[11:0] in,input wire[3:0] shift);
wire[11:0] stage1,stage2,stage3;
assign stage1 = (shift[0]) ? {in[10:0],1'b0} : in;
assign stage2 = (shift[1]) ? {stage1[9:0],2'b0} : stage1;
assign stage3 = (shift[2]) ? {stage2[7:0],4'b0} : stage2;
assign out = (shift[3]) ? {stage3[3:0],8'b0} : stage3;
endmodule

module Fpa(output wire[15:0] out,input wire[15:0] ain,input wire[15:0] bin,input wire mod_op);
wire[15:0] a,b,maddout;
wire[4:0] ediff;

Find_Big_diff diff_calc(a,b,ediff,ain,bin);

wire[4:0] ea,eb;
wire[9:0] ma,mb;
wire[10:0] smb;

assign ea = a[14:10];
assign eb = b[14:10];

assign ma = a[9:0];
assign mb = b[9:0];

B_Shifter_R shifter_r(smb,{eb[0]|eb[1]|eb[2]|eb[3]|eb[4],mb},ediff);
wire op;
assign op = b[15] ^ a[15] ^ mod_op;
Adder #(.wd(16),.itr(4)) add(.overflow(),.sum(maddout),.a({5'b0,ea[0]|ea[1]|ea[2]|ea[3]|ea[4],ma}),.b({5'b0,smb}),.cin(op),.ctr(op));

wire[3:0] shiftl;
wire[1:0] j;
assign j = maddout[11:10];
wire invl;
Lead_zero lz(shiftl,invl,maddout[9:0]);
wire[3:0] exp_norm;
wire[7:0] normExp;
wire[4:0] expOut;
wire norm_op;
assign exp_norm = (maddout[11])? 4'b1:((maddout[10])?  4'b0 : ~(invl)?shiftl:4'b0 );
assign norm_op = (maddout[11])? ((maddout[10])? 1'b0 : 1'b0 ):((maddout[10])? 1'b1 : 1'b1 );
Adder #(.wd(8),.itr(3)) addexp(.overflow(),.sum(normExp),.a({3'b0,ea}),.b({4'b0,exp_norm}),.cin(norm_op),.ctr(norm_op));
wire signOut;
wire[9:0] manOut;
wire[11:0] manshift;
wire[14:0] numOut;
wire expDiffIsZero,manDiffIsZero,manAIsZero,manBIsZero,manOutIsZero;
wire inIsDenorm,inIsInf,inIsNaN,zero,expAIsOne,expAIsZero,expBIsOne,expBIsZero;
wire aIsInf,aIsNaN,bIsInf,bIsNaN,expOutIsOne,expOutIsZero,outIsDenorm,outIsInf,overflow,naN,underflow;

assign expDiffIsZero = ~|ediff;
assign manDiffIsZero = ~|maddout[11:0];

assign signOut = a[15];
assign expOut = normExp[4:0];
B_Shifter_L shifter_l(manshift,maddout[11:0],exp_norm);
assign manOut = (norm_op)? manshift:maddout[10:1];
assign numOut = { expOut , manOut };
// Input exception signals
assign expAIsOne = &a[14:10];
assign expBIsOne = &b[14:10];
assign expAIsZero = ~|a[14:10];
assign expBIsZero = ~|b[14:10];
assign manAIsZero = ~|a[9:0];
assign manBIsZero = ~|b[9:0];
assign aIsNaN = expAIsOne & ~manAIsZero ;
assign bIsNaN = expBIsOne & ~manBIsZero ;
assign aIsInf = expAIsOne & manAIsZero ;
assign bIsInf = expBIsOne & manBIsZero ;
assign inIsNaN = aIsNaN | bIsNaN | ( aIsInf & bIsInf & op );
assign inIsInf = aIsInf | bIsInf ;
assign inIsDenorm = expAIsZero | expBIsZero ;
assign zero = expDiffIsZero & manDiffIsZero & op ;
// Output exception signals
assign expOutIsOne = & expOut ;
assign expOutIsZero = ~|expOut ;
assign manOutIsZero = ~|manOut ;
assign outIsInf = expOutIsOne & ~normExp[5];
assign outIsDenorm = expOutIsZero | normExp[5];
// Final exception signals
assign overflow = ( inIsInf | outIsInf ) & ~zero ;
assign naN = inIsNaN ;
assign underflow = inIsDenorm | outIsDenorm | zero ;
assign out = (naN)?16'h7fff:((overflow)?{signOut,15'b111110000000000}:((underflow)?16'b0:{signOut,numOut}));
//output_eval out_eval(out,underflow,overflow,naN,numOut,signOut);
// Choose output from exception signals
//if(naN)
//    assign out=16'h7fff;
//else if(overflow)
//    assign out = {signOut,15'b111110000000000};
//else if(underflow)
//    assign out = 16'b0;
//else
//    assign out = {signOut ,numOut};
//assign out = (naN) ? (16'h7fff) : ((overflow) ? ({signOut,15’b111110000000000}) : ((underflow) ? (16’b0) : ({signOut,numOut})));
//case(2'b{maddout[11],maddout[10]})
//    {2'b00}:assign exp_norm= ~(invl)?shiftl:4'b0;
//    {2'b01}:assign exp_norm= 4'b0;
//    {2'b10}:assign exp_norm= 4'b1;
//    {2'b11}:assign exp_norm= 4'b1;
//endcase
endmodule

