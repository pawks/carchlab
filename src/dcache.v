module dcache(
    input wire[`WordSize-1:0] dataIn,
    output reg[`WordSize-1:0] dataOut,
    output reg ready,
    output reg[`Addr-1:0] memAddr,
    output reg memEn,
    output reg memReadWrite,
    output reg[(`WordSize*`WordsInBlock)-1:0] memReg,
    input wire memReady,
    input wire[(`WordSize*`WordsInBlock)-1:0] blockFromMemory,
    input wire en,
    input wire opSel,//1-Write  0-Read
    input wire[`Addr-1:0] wordAddr,
    input wire reset,
    input wire clk
);

wire[`Index-1:0] line;
wire[`Offset-1:0] word;
wire[`Tag-1:0] tag;

reg[`WordSize-1:0] dcachemem[0:`Blocks-1][0:`WordsInBlock-1];
reg[`Tag-1:0] tagReg[0:`Blocks-1];
reg valid[0:`Blocks-1],dirty[0:`Blocks-1];

wire hitMiss;

assign line = wordAddr[`LStrobe];
assign word = wordAddr[`WStrobe];
assign tag = wordAddr[`TStrobe];

assign hitMiss = ~|(tagReg[line] ^ tag) & valid[line];//hitMiss
//Cache controller declarations
reg[1:0] state;//0-idle/hit 1-missDirty 2-missClean


//Reset- 0 - 1
genvar i;
generate
    for(i=0;i<`Blocks;i=i+1)begin
        always@(posedge clk)
            if(reset)begin
                valid[i]=1'b0;
                dirty[i]=1'b0;
            end
            
    end
endgenerate

always@(posedge clk)
    if(reset)begin
        state<=2'b00;
        memEn<=1'b0;
    end


//hit
// always@(posedge clk)
//     if(en & hitMiss & ~state[0] & ~state[1] & opSel)begin
//         dcachemem[line][word]<=data;
//         dirty[line]=1'b1;
//     end

// always@(negedge clk)
//     if(en & hitMiss & ~state[0] & ~state[1] & ~opSel)
//         dataOut<=dcachemem[line][word];

// always@(posedge clk)begin
//     if(state==2'b10)
//         if(dirty[line])begin
//             memAddr_str<=wordAddr;
//             memEn_str<=1'b0;
//             memReadWrite<=1'b1;
            
//         end


always@(posedge clk)
if(en)
    case(state)
        2'b00:
            if(hitMiss)begin
                if(opSel)begin
                    dcachemem[line][word]<=dataIn;
                    dirty[line]=1'b1;
                end
                else
                    dataOut<=dcachemem[line][word];
                ready<=1'b1;
            end
            else begin
                if(dirty[line])begin
                    memAddr<={tagReg[line],wordAddr[line],`BlockMask};
                    memReg = {dcachemem[line][15],dcachemem[line][14],dcachemem[line][13],dcachemem[line][12],dcachemem[line][11],
                      dcachemem[line][10],dcachemem[line][9],dcachemem[line][8],dcachemem[line][7],dcachemem[line][6],
                      dcachemem[line][5],dcachemem[line][4],dcachemem[line][3],dcachemem[line][2],dcachemem[line][1],
                      dcachemem[line][0]};
                    memEn<=1'b1;
                    memReadWrite<=1'b1;
                    state<=2'b01;
                    valid[line]<=1'b0;                
                end
                else begin
                    memAddr<={tag,wordAddr[line],`BlockMask};
                    memEn<=1'b1;
                    memReadWrite<=1'b0;
                    state<=2'b10;
                    valid[line]<=1'b0;
                end
                ready<=1'b0;
            end
        2'b01:
            if(memReady)begin
            memAddr<={tag,wordAddr[line],`BlockMask};
            memEn<=1'b1;
            memReadWrite<=1'b0;
            valid[line]<=1'b0;
            state<=2'b10;
            end
        2'b10:
            if(memReady)begin
            memEn<=1'b0;
            {dcachemem[line][15],dcachemem[line][14],dcachemem[line][13],dcachemem[line][12],dcachemem[line][11],
            dcachemem[line][10],dcachemem[line][9],dcachemem[line][8],dcachemem[line][7],dcachemem[line][6],
            dcachemem[line][5],dcachemem[line][4],dcachemem[line][3],dcachemem[line][2],dcachemem[line][1],
            dcachemem[line][0]}=blockFromMemory;
            tagReg[line]=tag;
            dirty[line]=1'b0;
            valid[line]=1'b1;
            state<=2'b00;
            end
    endcase
else
    ready=1'b0;

// always@(reset)
//     $display("0:%b",valid[0]);

// always@(*)
//     $display("cache:%b hit:%b valid:%b line:%b",wordAddr,hitMiss,valid[line],line);
endmodule
