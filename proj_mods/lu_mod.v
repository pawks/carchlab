`include "logicUnit.v"
module testbench();
reg[15:0] a,b;
reg op;
wire[15:0] out;
`ifdef NAND
    logicNAND al(out,a,b);
`endif
`ifdef CMP
   logicNEG al(out,a);
`endif
`ifdef XOR
    logicXOR al(out,a,b);
`endif
`ifdef SHR
    shRight al(out,a,b);
`endif
`ifdef LHR
    shLeft al(out,a,b);
`endif
initial begin
    a<=`A;
    b<=`B;
    #10
    $display(" %b ",out);
    $finish;

end
endmodule