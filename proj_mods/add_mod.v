`include "Parallel_Prefix_CLA.v"
module testbench();
    reg[15:0] a,b;
    reg cin,ctr;
    wire cout;
    wire[15:0] sum;
    Adder #(16,4) add1 (cout,sum,a,b,cin,ctr);
    initial begin
      ctr = `OP;
      a=`A;
      b=`B;
      cin=`C;
      #5
      $display(" %b ",out);
      $finish;
    end
endmodule