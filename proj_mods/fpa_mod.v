`include "fpa.v"
`include "Parallel_Prefix_CLA.v"

module testbench();
reg[15:0] a,b;
reg op;
wire[15:0] out;
wire[7:0] sum;

Fpa al(out,a,b,op);
initial begin
    a<=`A;
    b<=`B;
    op<=`OP;
    #10
    $display(" %b ",out);
    $finish;

end
endmodule