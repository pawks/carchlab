`include "fpm.v"
`include "Parallel_Prefix_CLA.v"
`include "wallace_multiplier.v" 

module testbench();
reg[15:0] a,b;
reg op;
wire[15:0] out;

Fpm al(out,a,b);
initial begin
    a<=`A;
    b<=`B;
    #10
    $display(" %b ",out);
    $finish;
end
endmodule