`include "wallace_multiplier.v"
`include "Parallel_Prefix_CLA.v"
module testbench();

    reg [15:0] a, b;
    wire [31:0] p;
    Multiplier mul(.a(a), .b(b), .msb(p[31:16]),.lsb(p[15:0]));
    initial begin
    a<=`A;
    b<=`B;
    #20;
    $display(" %b ",p[15:0]);
    $finish;
    end
endmodule