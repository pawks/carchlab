import os
import sys
import subprocess
import shlex
filename = "input_inst.asm" #file containing the program to execute
class FUS:
	def __init__(self, name, delay, busy = False, op = None, fi = None, fj = None, fk = None, qj = None, qk = None, rj = None, rk = None):
		self.name = name
		self.busy = busy
		self.op = op
		self.fi = fi
		self.fj = fj
		self.fk = fk
		self.qj = qj
		self.qk = qk
		self.rj = rj
		self.rk = rk
		self.delay = delay
	def __str__ (self):
		return "NAME = %s,Delay = %s, BUSY = %s,OP = %s, FI = %s, FJ = %s, FK = %s, QJ = %s, QK = %s, RJ = %s, RK = %s" % (self.name,self.delay , self.busy, self.op, self.fi, self.fj, self.fk, self.qj, self.qk, self.rj, self.rk)
	def rst(self):
		self.busy = False
		self.op = None
		self.fi = None
		self.fj = None
		self.fk = None
		self.qj = None
		self.qk = None
		self.rj = None
		self.rk = None

def addmod(op,a,b,c):
	cmd = "iverilog -D A="+ str(int(a,2)) +" -D B="+ str(int(b,2)) +" -D C="+ str(int(c,2)) +" -D OP="+ str(int(op,2)) +" -I ./proj_mods/  -I ./src/ -s testbench ./proj_mods/add_mod.v -o a.out"
	cmd = shlex.split(cmd)
	subprocess.call(cmd)
	p = subprocess.Popen(["vvp","a.out"],stdout=subprocess.PIPE)
	a = p.communicate()
	out = str(a[0]).split(" ", 2)
	return str(out[1])

def fpamod(op,a,b):
	cmd = "iverilog -D A="+ str(int(a,2)) +" -D B="+ str(int(b,2)) +" -D OP="+ str(int(op,2)) +" -I ./proj_mods/  -I ./src/ -s testbench ./proj_mods/fpa_mod.v -o a.out"
	cmd = shlex.split(cmd)
	subprocess.call(cmd)
	p = subprocess.Popen(["vvp","a.out"],stdout=subprocess.PIPE)
	a = p.communicate()
	out = str(a[0]).split(" ", 2)
	return str(out[1])

def mulmod(a,b):
	# print(a,b)
	cmd = "iverilog -D A="+ str(int(a,2)) +" -D B="+ str(int(b,2)) +" -I ./proj_mods/  -I ./src/ -s testbench ./proj_mods/multiplier_mod.v -o a.out"
	#print("wtm:"+str(cmd))
	cmd = shlex.split(cmd)
	subprocess.call(cmd)
	p = subprocess.Popen(["vvp","a.out"],stdout=subprocess.PIPE)
	a = p.communicate()
	out = str(a[0]).split(" ", 2)
	return str(out[1])

def fpmmod(a,b):
	# print("fpm")
	# print(a,b)
	cmd = "iverilog -D A="+ str(int(a,2)) +" -D B="+ str(int(b,2)) +" -I ./proj_mods/  -I ./src/ -s testbench ./proj_mods/fpm_mod.v -o a.out"
	#print(cmd)
	cmd = shlex.split(cmd)
	subprocess.call(cmd)
	p = subprocess.Popen(["vvp","a.out"],stdout=subprocess.PIPE)
	a = p.communicate()
	out = str(a[0]).split(" ", 2)
	return str(out[1])

def lumod(op,a,b):
	if(b==None):
		b="00000"
	cmd = "iverilog -D A="+ str(int(a,2)) +" -D B="+ str(int(b,2)) +" -D "+ op +" -I ./proj_mods/  -I ./src/ -s testbench ./proj_mods/lu_mod.v -o a.out"
	# print("log:"+str(cmd))
	cmd = shlex.split(cmd)
	subprocess.call(cmd)
	p = subprocess.Popen(["vvp","a.out"],stdout=subprocess.PIPE)
	a = p.communicate()
	out = str(a[0]).split(" ", 2)
	return str(out[1])

def read_instructions(filename):
	inst = []
	with open(filename) as f:
		inst = f.readlines()
	inst = [x.strip('\n') for x in inst]
	inst = [x.strip(';') for x in inst]
	inst_split = [", ".join(x.split(" ", 1)) for x in inst]
	inst_split = [[x.strip() for x in y.split(',')] for y in inst_split]
	inState = []
	n = 1
	for ins in inst_split:
		fu = evalopfu(ins[0])
		op = ins[0]
		#print(fu)
		if(ins[0] == "HLT"):
			rdst = ""
			rsc1 = ""
			rsc2 = ""
		elif(ins[0] == "CMP"):
			rdst = ins[1]
			rsc1 = ins[2]
			rsc2 = None
		elif(ins[0] == "LDR"):
			rdst = ins[1]
			rsc1 = ins[2]
			rsc2 = None
		elif(ins[0] == "STR"):
			rdst = ins[2]
			rsc1 = ins[1]
			rsc2 = None
		else:
			rdst = ins[1]
			rsc1 = ins[2]
			rsc2 = ins[3]
		inState.append({"inum":n,"fu":fu,"op":op,"rdst":rdst,"rsc1":rsc1,"rsc2":rsc2,"issue":-1,"readOp":-1,"execute":-1,"writeBack":-1,"state":"Initial","outval":""})
		n = n+1
		# print(inst)
	return inState

def evalopfu(op):
	f=""
	if(op == "LDR" or op == "STR"):
		f = "LDR"
	elif(op == "NAND" or op == "CMP" or op == "XOR" or op == "SHR" or op == "LHR"):
		f = "LU" 
	elif(op == "ADD" or op == "ADC" or op == "SUB" or op == "SBB"):
		f = "ADD"
	elif(op == "FADD" or op == "FSUB"):
		f = "FADD"
	elif(op == "MUL"):
		f = "MUL"
	elif(op == "FMUL"):	
		f = "FMUL"
	return f

def execin(ins,Rval):
	a = Rval[ins["rsc1"]]
	b = Rval[ins["rsc2"]]
	if(ins["op"] == "ADD"):
		op = "0"
		c = "0"
		return addmod(op,a,b,c)
	elif(ins["op"] == "ADC"):
		op = "0"
		c = "1"
		return addmod(op,a,b,c)
	elif(ins["op"] == "SUB"):
		op = "1"
		c = "1"
		return addmod(op,a,b,c)
	elif(ins["op"] == "SBC"):
		op = "1"
		c = "0"
		return addmod(op,a,b,c)
	elif(ins["op"] == "FADD"):
		op = "0"
		return fpamod(op,a,b)
	elif(ins["op"] == "FSUB"):
		op = "1"
		return fpamod(op,a,b)
	elif(ins["op"] == "FMUL"):
		return fpmmod(a,b)
	elif(ins["op"] == "MUL"):
		return mulmod(a,b)
	elif(ins["fu"] == "LU"):
		op = ins["op"]
		return lumod(op,a,b)

def intro():
	print("############ 16Bit processor using Scoreboarding ############")
	print("Author-S Pawan Kumar - CED16I043")
	print("Clock Frequency = 4.34GHz")
	print()
	print("**************************************************************************************************")
	print("############ Functional Units Present ############")
	print("Recursive Doubling Based CLA-16bits")
	print("Wallace Tree Multiplier - input-16bits output=lower 16bits")
	print("Floating Point Adder - 16bit")
	print("Floating Point Multplier - 16bit")
	print("Logic Unit")
	print("Load Store Unit")
	print()
	print("**************************************************************************************************")

if(__name__ == '__main__'):
	inState = read_instructions(filename)
	# for ins in inState:
	# 	print(ins)
	running = True
	clk = 1
	intro()
	RRS = {"R0" : None, "R1" : None, "R2" : None, "R3" : None, "R4" : None, "R5" : None, 
		"R6" : None, "R7" : None, "R8" : None, "R9" : None, "R10" : None, "R11" : None, "R12" : None}
	Rval = {"R0" : "0000000000000000", "R1" : "0000000000000001", "R2" : "0000000000000010", "R3" : "0000000000000000", "R4" : "0000000000000000", "R5" : "0000000000000000", 
		"R6" : "0000000000000000", "R7" : "0000000000000000", "R8" : "0000000000000000", "R9" : "0000000000000000", "R10" : "0000000000000000", "R11" : "0000000000000000", "R12" : "0000000000000000"}
	FU = {"MUL" : FUS("MUL", 5), "FADD" : FUS("FADD", 6), "FMUL" : FUS("FMUL", 5), "ADD" : FUS("ADD", 1), "LDR" : FUS("LDR", 1), "LU" : FUS("LU", 1)}
	MEM = {
		"00001111":"00000000000000001",
		"00010000":"00000000000000010",
		"00010001":"00000000000000000"
	}
	issQ = []
	readOpQ = []
	exQ = []
	writeBackQ = []
	completeQ = []
	for ins in inState:
		ins["state"]="issue"
		issQ.append(ins)
		# print(ins)
	# print()
	# print("EXECUTION STARTS")
	while(running):
		if(issQ):
			ins = issQ[0]
			if ins["op"]!="HLT":
				if(not FU[ins["fu"]].busy and (RRS[ins["rdst"]]==None)):
					FU[ins["fu"]].busy = True
					FU[ins["fu"]].op = ins["op"]
					FU[ins["fu"]].fi = ins["rdst"]
					FU[ins["fu"]].fj = ins["rsc1"]
					FU[ins["fu"]].fk = ins["rsc2"]
					if(ins["op"] != "LDR"):
						FU[ins["fu"]].qj = RRS[ins["rsc1"]]
						FU[ins["fu"]].rj = True if(RRS[ins["rsc1"]]==None) else False
					else:
						FU[ins["fu"]].qj = None
						FU[ins["fu"]].rj = True
					if(ins["rsc2"]!=None):
						FU[ins["fu"]].qk = RRS[ins["rsc1"]]
						FU[ins["fu"]].rk = True if(RRS[ins["rsc1"]]==None) else False
					else:
						FU[ins["fu"]].qk = ins["rsc1"]
						FU[ins["fu"]].rk = True
					RRS[ins["rdst"]] = ins["fu"]	
					ins["issue"] = clk
					ins["state"] = "readOp"
					issQ.remove(ins)
					readOpQ.append(ins)
			else:
				issQ.remove(ins)
				readOpQ.append(ins)
		# running = False
		rdl = []
		for ins in readOpQ:
			if ins["op"]!="HLT":
				if(ins["issue"]<clk):
					if(FU[ins["fu"]].rk and FU[ins["fu"]].rj):
						#print("kk:"+str(ins))
						FU[ins["fu"]].qk = None
						FU[ins["fu"]].qj = None
						FU[ins["fu"]].rk = False
						FU[ins["fu"]].rj = False
						ins["state"] = "execute"
						ins["readOp"] = clk
						if(ins["fu"]!="LDR"):
							ins["outval"] = execin(ins,Rval)
						elif(ins["op"] == "LDR"):
							ins["outval"] = MEM[ins["rsc1"]]
						elif(ins["op"] == "STR"):
							MEM[ins["rdst"]] = Rval[ins["rsc1"]]
						# print("KKK:"+ins["outval"])
						rdl.append(ins)
						exQ.append(ins)
		for ins in rdl:
			readOpQ.remove(ins)
		if(readOpQ):
			if(readOpQ[0]["op"]=="HLT"):
				ins = readOpQ[0]
				readOpQ.remove(ins)
				exQ.append(ins)

		exl = []
		for ins in exQ:
			if ins["op"]!="HLT":
				if(ins["readOp"]+int(FU[ins["fu"]].delay) == clk):
					ins["state"] = "writeBack"
					ins["execute"] = clk
					exl.append(ins)
					writeBackQ.append(ins)
		for ins in exl:
			exQ.remove(ins)
		if(exQ):
			if(exQ[0]["op"]=="HLT"):
				ins = exQ[0]
				exQ.remove(ins)
				writeBackQ.append(ins)
		
		wbl = []
		for ins in writeBackQ:
			if ins["op"]!="HLT":
				if(ins["execute"]<clk):
					write = True
					for f in FU:
						if(FU[f].rk and FU[f].fk == FU[ins["fu"]].fi) or (FU[f].rj and FU[f].fj == FU[ins["fu"]].fi):
							write=False
					if(write):
						for f in FU:
							if(FU[f].qj == ins["fu"]):
								FU[f].rj = True
							if(FU[f].qk == ins["fu"]):
								FU[f].rk = True
						RRS[FU[ins["fu"]].fi] = None
						ins["writeBack"] = clk
						ins["state"] = "Completed"
						# print(Rval)
						if(ins["op"]!="STR"):
							Rval[FU[ins["fu"]].fi] = ins["outval"]
						# print(FU[ins["fu"]].fi)
						# print(Rval)
						# del ins["outval"]
						FU[ins["fu"]].rst()
						wbl.append(ins)
						completeQ.append(ins)
		for ins in wbl:
			writeBackQ.remove(ins)
		if(writeBackQ):
			if(writeBackQ[0]["op"]=="HLT"):
				ins = writeBackQ[0]
				writeBackQ.remove(ins)
				completeQ.append(ins)
				running = False
		# print()
		# print(clk)
		# print("FU")
		# for f in FU:
		# 	print(FU[f])
		# print("Register Status")
		# print(RRS)
		# print("IssueQ")
		# for ins in issQ:
		# 	print(ins)
		# print("ReadQ")
		# for ins in readOpQ:
		# 	print(ins)
		# print("ExecQ")
		# for ins in exQ:
		# 	print(ins)
		# print("WriteBackQ")
		# for ins in writeBackQ:
		# 	print(ins)
		if(not issQ and not readOpQ and not exQ and not writeBackQ) or (clk>50):
			running = False
		clk=clk+1
	#print("Final Output")
	print("Num\tIssue\tReadOp\tExecute\tWriteBack")
	for ins in completeQ:
		print(str(ins["inum"])+" \t"+str(ins["issue"])+"\t"+str(ins["readOp"])+"\t"+str(ins["execute"])+"\t"+str(ins["writeBack"]))
	print()
	for r in Rval:
		print(str(r)+":"+str(Rval[r]))
	print()
	for k in MEM:
		print(str(k)+":"+str(MEM[k]))