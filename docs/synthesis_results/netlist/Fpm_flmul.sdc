# ####################################################################

#  Created by Genus(TM) Synthesis Solution GENUS15.23 - 15.20-s040_1 on Sat Aug 20 16:39:33 +0530 2016

# ####################################################################

set sdc_version 1.7

set_units -capacitance 1000.0fF
set_units -time 1000.0ps

# Set the current design
current_design Fpm

set_clock_gating_check -setup 0.0 
set_wire_load_mode "segmented"
set_wire_load_selection_group "WireAreaForZero" -library "tcbn45gsbwpbc0d88_ccs"
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/BHDBWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/BUFFD20BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/BUFFD24BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/BUFTD20BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/BUFTD24BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/CKBD20BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/CKBD24BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/CKLHQD20BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/CKLHQD24BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/CKLNQD20BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/CKLNQD24BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/CKND20BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/CKND24BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/DCCKBD20BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/DCCKND20BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GAN2D1BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GAN2D2BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GAOI21D1BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GAOI21D2BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GAOI22D1BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GBUFFD1BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GBUFFD2BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GBUFFD3BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GBUFFD4BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GBUFFD8BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GDCAP10BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GDCAP2BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GDCAP3BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GDCAP4BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GDCAPBWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GDFCNQD1BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GDFQD1BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GFILL10BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GFILL2BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GFILL3BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GFILL4BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GFILLBWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GINVD1BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GINVD2BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GINVD3BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GINVD4BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GINVD8BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GMUX2D1BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GMUX2D2BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GMUX2ND1BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GMUX2ND2BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GND2D1BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GND2D2BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GND2D3BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GND2D4BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GND3D1BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GND3D2BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GNR2D1BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GNR2D2BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GNR3D1BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GNR3D2BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GOAI21D1BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GOAI21D2BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GOR2D1BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GOR2D2BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GSDFCNQD1BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GTIEHBWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GTIELBWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GXNR2D1BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GXNR2D2BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GXOR2D1BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/GXOR2D2BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/INVD20BWP]
set_dont_use [get_lib_cells tcbn45gsbwpbc0d88_ccs/INVD24BWP]
