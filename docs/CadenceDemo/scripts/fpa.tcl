# #####################################################
# Script for Cadence RTL Compiler synthesis      
# Use with syn-rtl -f rtl-script
# Replace items inside <> with your own information
# #####################################################
# Set the search paths to the libraries and the HDL files
# Remember that "." means your current directory. Add more directories
# after the . if you like. 



#*********************************************************
#*   below here need to be changed for path & name...          *
#*********************************************************
set username hprcse			;# system username
set desn_foldr synthesis		;# name of design folder
set hdl_foldr src
set myFiles fpa.v	;# All your HDL files
set basename Fpa				;# name of top level module
set hdl v2001				;# v2001 : verilog        vhdl : vhdl         sv : system verilog
set runname fpa           		;# name appended to output files
set myClk clk_virt             		;# clock name
set level medium			;# Synthesize optimization level
set lib tcbn45gsbwpbc0d88_ccs.lib	;# Technology Library
#set lib slow_vdd1v0_basicCells.lib	;# Technology Library



#*********************************************************
#*   below here need to be changed for values...          *
#*********************************************************
#set myPeriod_ps 10                	;# Clock period in ps
#set myInDelay_ns <num>          	;# delay from clock to inputs valid
#set myOutDelay_ns <num>          	;# delay from clock to output valid
#set riseFall <num>		  	;# fall/rise 400ps set 0.4
set infoLevel 6		  		;# information level for the design as default set 6



#*********************************************************
#*   below here may not be changed...		         *
#*   Apply Constraints and generate clocks               *
#*********************************************************

set libraryPath /home/cadence/cadence_tools/genus_lib
set hdlPath /home/${username}/Desktop/ced16i043_carch/${hdl_foldr}
set netlistPath /home/${username}/Desktop/ced16i043_carch/${desn_foldr}/netlist
set reportPath /home/${username}/Desktop/ced16i043_carch/${desn_foldr}/report
set myFiles [list ${myFiles}]

#set_attr hdl_vhdl_read_version 1993
#set_attribute hdl_language vhdl
#set_attr hdl_language sv



set_attribute hdl_search_path ${hdlPath} 
set_attribute lib_search_path ${libraryPath}
set_attribute library [list ${lib}]
set_attribute information_level ${infoLevel} 

# Analyze and Elaborate the HDL files
read_hdl -$hdl ${myFiles}
#read_sdc > /home/hprcse/Desktop/ced1i043_carch/CadenceDemo/constraints_top.sdc
elaborate ${basename}

#create_clock -name $myClk -period ${myPeriod_ps}
#set clock [define_clock -period ${myPeriod_ps} -name ${myClk} [clock_ports]]	
#external_delay -input $myInDelay_ns -clock ${myClk} [find / -port ports_in/*]
#external_delay -output $myOutDelay_ns -clock ${myClk} [find / -port ports_out/*]

# Sets transition to default values for Synopsys SDC format, 
# fall/rise 400ps
#dc::set_clock_transition ${riseFall} $myClk

# check that the design is OK so far
check_design -unresolved
report timing -lint

# Synthesize the design to the target library
synthesize -to_mapped -effort ${level}

# Write out the reports
report timing > ${reportPath}/${basename}_${runname}_timing.rep
report gates  > ${reportPath}/${basename}_${runname}_cell.rep
report power  > ${reportPath}/${basename}_${runname}_power.rep

# Write out the structural Verilog and sdc files
write_hdl -mapped >  ${netlistPath}/${basename}_${runname}.v
write_sdc >  ${netlistPath}/${basename}_${runname}.sdc

gui_show
