#*****************************************************************************************
#*                         below here shouldn't need to be changed...                    *
#*****************************************************************************************
# Replace items inside <> with your own information
# Change user_name as your hpc_user_account_name
# Change Roll_number with your rollnumber
# Change designTOP with your top_module name
# Change top.v to your top_module name with extension, similar with all submodules sub.v
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::#


#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::#
set_attr lib_search_path /home/cadence/cadence_tools/genus_lib							
#set_attr library /soft/cadence/cadence_tools/genus_lib/slow_vdd1v0_basicCells.lib
set_attr library /home/cadence/cadence_tools/genus_lib/tcbn45gsbwpbc0d88_ccs.lib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::#

#::::::::::::::::::::::::::::::::::::::    Edit   :::::::::::::::::::::::::::::::::::::::#
set_attr hdl_search_path /home/hprcse/Desktop/ced16i043_carch/src
read_hdl {Parallel_Prefix_CLA.v}
#read_sdc > /home/iiitdm/Desktop/work/Roll_number/designTOP.sdc
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::#


#:::::::::::::::::::::::::::::::::::    Don't Edit  ::::::::::::::::::::::::::::::::::::#
elaborate
synthesize -to_mapped -effort medium
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::#


#::::::::::::::::::::::::::::::::::::::    Edit   :::::::::::::::::::::::::::::::::::::::#
write_hdl > /home/hprcse/Desktop/ced16i043_carch/synthesis/designTOP_netlist.v 
#write_sdc > /home/iiitdm/Desktop/work/Roll_number/designTOP_sdc.sdc
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::#


#:::::::::::::::::::::::::::::::::::    Don't Edit  ::::::::::::::::::::::::::::::::::::#
report timing
report power
report area
gui_show
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::#
