

																				IN CLIENT MACHINE (YOUR MACHINE)
					---------------------------------------------------------------------------------------------------------------------------------------------------
					|									INSTALL PUTTY FOR BOTH WINDOWS AND Linux																		|
					|										INSTALL XMING FOR WINDOWS																					|
					|											IF REQUIRED SET PATH VARIABLE FOR WINDOWS																|
					|												IF REQUIRED EXPORT DISPLAY SERVER ON BASHRC FOR LINUX												|
					|													REPLCAE INSIDE <> WITH CORRESPONDING PATH OR NAME												|																																					
 ___________________|___________________________________________________________________________________________________________________________________________________|___________________
|		Step				|	Windows																		|	Linux																		|
|	1.	Check Connectivity	|	ping 172.16.1.80															|	ping 172.16.1.80															|
|	2.	Create Files		|	save all files including rc_script.tcl in folder							|	save all files including rc_script.tcl in folder							|
|	3.	Copy files			|	scp -r <source_folder_path>\(* or filename.extention) 						|	scp -r <source_folder_path>/(* or filename.extention) 						|
|							|			<roll_number>@172.16.1.80:/home/<roll_number>/Desktop/work/<genus>	|			<roll_number>@172.16.1.80:/home/<roll_number>/Desktop/work/<genus>	|
|	4.	Remote Connection	|	putty -X <roll_number>@172.16.1.80 -pw <pass_word>							|	ssh -X <roll_number>@172.16.1.80 -pw <pass_word>							|
 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 


																			IN HOST MACHINE (HPC MACHINE - SSH TERMINAL)
 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
|																																															|
|																CONFIRM YOU ARE TYPING THESE COMMANDS IN HOST MACHINE TERMINAL																|
|									        EDIT .TCL FILE ACCORDING TO THE DESIGN FOLDER NAME AND PATH (REFER GENUS.PDF FOR .tcl AND .sdc COMMANDS)										|
|___________________________________________________________________________________________________________________________________________________________________________________________|
|		Step				|	Commands																																					|
|	1.	Check Connectivity	|	ping 172.16.1.72														Successfull ping without loss														|
|	2.	Check files			|	ls <path_mentioned_when_scp_in client>									Copied filed shold present															|
|	3.	Check Terminal path	|	pwd																		Path should be /soft/cadence/log/													|
|	4.	Set PATH (optional)	|	cd /soft/cadence/log/													if path not correct then set														|
|	5.	Invoke shell		|	csh																		{You should see Welcome to cadence tools message)									|
|	6.	Check with TA's		|																			If prvious message not visible check with your TA's									|
|	7.	Invoke tools		|	genus -legacy_ui														If licence error or abnormal exit inform to TA										|
|	8.	Source script file	|	source /home/<roll_number>/Desktop/work/<genus>/rc.script.tcl			If any error goto step 9 else step 10												|
|	9.	Error checking		|	gedit /home/<roll_number>/Desktop/work/<genus>/<filename.extension>		Edit to rectify the error mentioned in step 8 and save the file	& repeat step 8		|
|	10. Check the Schematic	|																			If unconnected ports present check design with schematic. (stpe 12 then stpe 3)		|
|	11.	Report				|																			No error in step 10 -- Schematic, Timing, power, area report along with screenshot	|
|	12.	Exit from genus		|	exit																	it should exit from genus. bash terminal path should be step 3						|
|	13. Exit from host		|	exit																	type exit untill putty terminal goes for inactive state by showing logout message	|
 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


																			POSSIBLE ERRORS
 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
|																																															|
|																		CONFIRM YOURSELF ERROR MESSAGE FOR CLIENT OR HOST																	|
|															BASED ON THE ERROR CHOOSE THE CLIENT / HOST TERMINAL FOR CHECKING ERROR															|
|___________________________________________________________________________________________________________________________________________________________________________________________|
|		ERROR				|	REASON																																						|
|	1.	ping				|	client & host not connected in same network										Ask TA																		|
|	2.	scp					|	source file may not present or destination path may not present					Correct the path and name by yourself										|
|	3.	putty/ssh(port 22)	|	restart ssh server or wait for 10-15 min toset by itself						Correct the path and name by yourself										|
|	4.	remote display		|	export display variable in bashrc												Correct the path and name by yourself										|
|	5.	genus				|	Licence server may not present in same network									Ask TA																		|
|	6.	design error in .v	|	may be syntax or functional error												Correct the path and name by yourself if youcan't Ask TA					|
 -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------