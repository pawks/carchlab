`include "../src/Parallel_Prefix_CLA.v"
`include "../src/imem.v"
`include "../src/clock.v"
`include "../src/ifu.v"
module test();
wire clk;
timer ti(clk);
reg en,rst;
wire[15:0] out;
ifu iunit(out,clk,rst,en);
reg[15:0] cnt;
initial begin
  en<=1;
  rst<=1;
  cnt<=0;
  #1
  rst<=0;
  #300
  $finish;
end
always@(posedge clk)
    if(~rst)
        cnt=cnt+16'b1;
always@(*)
    $display("t:%d data:%b",cnt,out);
endmodule