`include "../src/Parallel_Prefix_CLA.v"
module testbench();
    reg[15:0] a,b;
    reg cin;
    wire cout;
    wire[15:0] sum;
    Adder #(16,4) add1 (cout,sum,a,b,cin,ctr);
    initial begin
      $display("CLA with Parallel prefix tree and recurssive doubling\n\t1-sub\t0-add");
      a=16'hffff;
      b=16'h0001;
      cin=0;
      #5
      a=16'habcd;
      b=16'h0001;
      #5
      a=16'h0000;
      b=16'h0001;
      cin=1;
      #5;
      a=16'habcd;
      b=16'h0001;
      #10;
      $finish;
    end

    always@(cout or sum)
        $monitor("a:%h b:%h OP:%h overflow:%h sum:%h",a,b,cin,cout,sum);

endmodule