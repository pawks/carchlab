`include "../src/register_file.v"
`include "../src/clock.v"
module testbench();
wire clk;
timer ti(clk);
reg[3:0] r1_addr,r2_addr,wr_addr;
reg[15:0] wr_data;
wire[15:0] r1_data,r2_data;
reg ren,wen,ren1,ren2,rst;
reg_file r(clk,rst,ren1,ren2,wen,r1_addr,r2_addr,wr_addr,wr_data,r1_data,r2_data);
initial begin
    #1
    rst<=1;
    wen<=0;
    ren1<=0;
    ren2<=0;
    r2_addr<=4'b0000;
    r1_addr<=4'b0000;
    #2
    rst<=0;
    wen<=1;
    wr_addr<=4'b0000;
    wr_data<=16'hffff;
    #2
    wen<=1;
    wr_addr<=4'b0001;
    wr_data<=16'hfffe;
    #2
    wen<=1;
    wr_addr<=4'b0010; 
    wr_data<=16'hfffd;
    #2
    wen<=1;
    wr_addr<=4'b0011;
    wr_data<=16'hfffc;
    #2
    ren1<=1;
    r1_addr<=4'b0000;
    ren2<=1;
    r2_addr<=4'b0001;
    wen<=1;
    wr_addr<=4'b0100;
    wr_data<=16'hfffb;
    #2
    ren1<=1;
    r1_addr<=4'b0010;
    ren2<=1;
    r2_addr<=4'b0011;
    wen<=0;
    #2
    ren1<=1;
    r1_addr<=4'b0100;
    ren2<=0;
    #2
    $finish;
end
always @(posedge clk)
    $display("rst:%h ren1:%h ren2:%h wen:%h r1_addr:%h r2_addr:%h wr_addr:%h\nwr_data:%h r1_data:%h r2_data:%h",rst,ren1,ren2,wen,r1_addr,r2_addr,wr_addr,wr_data,r1_data,r2_data);
endmodule