module test();
wire clk;
timer ti(clk);
reg[15:0] addr,data_in;
reg opsel,en,reset;
wire[15:0] data_out,add_out;
wire[255:0] dintout,dintin;
wire dready,ready;
wire dop;
wire den;
dcache d(data_in,data_out,ready,add_out,den,dop,dintout,dready,dintin,en,opsel,addr,reset,clk);
memory m(,dintin,,dready,,add_out,dintout,,den,dop,clk);

initial begin
  reset<=1'b1;
  #2
  reset<=1'b0;
  opsel<=1'b0;
  addr<=16'b0;
  en<=1'b1;
  #100
  $finish;
end

always @(*)
    $display("reset:%b addr:%b ready:%b data:%b",reset,addr,ready,data_out);

endmodule
