`include "../src/fpm.v"
`include "../src/Parallel_Prefix_CLA.v"
`include "../src/wallace_multiplier.v" 

module test();
reg[15:0] a,b;
reg op;
wire[15:0] out;
wire[7:0] sum;

Fpm al(out,a,b);
initial begin
    a<=16'b0100011100100100;
    b<=16'b0100011011001100;
    #10
    a<=16'b0111111111111111;
    b<=16'b0100011011001100;
    #10
    a<=16'd8996;
    b<=16'd18212;
    #10
    a<=16'b0111110000000000;
    b<=16'b0111110000000000;
    #10
    a<=16'b0111110000000000;
    b<=16'b0111110000000000;
    #10
    a<=16'b0100011011001100;
    b<=16'b0100011011001100;
    #10
    a<=16'b0100011100100100;
    b<=16'b0;
    #10
    a<= 16'b0011110011001101;
    b<= 16'b0100001100110011;
    #10
    a<= 16'b0011110011001101;
    b<= 16'b0100001100110011;
    #10
    $finish;

end
always@(*)
    $monitor("a:%b b:%b out:%b",a,b,out);


endmodule