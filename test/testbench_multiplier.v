`include "../src/wallace_multiplier.v"
`include "../src/Parallel_Prefix_CLA.v"
module testbench();

    reg [15:0] a, b;
    wire [31:0] p;
    Multiplier mul(.a(a), .b(b), .msb(p[31:16]),.lsb(p[15:0]));
    initial begin
    a<=16'habcd;
    b<=16'hbcde;
    #20;
    end
    always@(*)
        $display("Input: %h %h p:%h",a,b,p);

endmodule