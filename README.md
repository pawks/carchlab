# CArchLab

1. 16-bit Recursive Doubling based Carry Lookahead Adder
2. 16-bit Wallace Tree Multiplier
3. 16-bit Floating Point Adder
4. 16-bit Floating Point Multiplier
5. Pipelining of Exp1 and Exp2
6. Instruction Fetch Unit
7. Instruction Formating and Decode Unit
8. Tomosulo Hardware Algorithm
9. Direct Memory Cache Design
10. Load Store Unit Design
11. Building Single Core Processor
12. MESI Protocol Implementation
13. Core Processor Implementation
