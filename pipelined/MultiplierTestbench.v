`include "WallaceMultiplier.v"
module MultiplierTestbench();

    reg [15:0] InputA, InputB;
    wire [31:0] Output;
    reg Clock;
    Multiplier mul(.A(InputA), .B(InputB), .C(Output), .clk(Clock));
    initial begin
    Clock<=1'b0;
    InputA<=16'habcd;
    InputB<=16'hbcde;
    #2;
    InputA<=16'hffff;
    InputB<=16'hffff;
    #2;
    InputA<=16'hb29d;
    InputB<=16'hffff;
    #2;
    InputA<=16'h6540;
    InputB<=16'h3216;
    #2;
    InputA<=16'h3265;
    InputB<=16'h2656;
    #2;
    InputA<=16'h0000;
    InputB<=16'h0000;
    #2;
    InputA<=16'habce;
    InputB<=16'hbcde;
    #2;
    InputA<=16'hfffe;
    InputB<=16'hffff;
    #2;
    InputA<=16'hb29e;
    InputB<=16'hffff;
    #2;
    InputA<=16'h654e;
    InputB<=16'h3216;
    #2;
    InputA<=16'h326e;
    InputB<=16'h2656;
    #2;
    InputA<=16'h000e;
    InputB<=16'h0000;
    #10000;
    $finish;
    end
    always@(InputA|Output)
        $display("Time: %h Input: %h %h Output:%h Clock:%b",$time,InputA,InputB,Output,Clock);

    always begin
    #1 Clock<=1'b1;
    #1 Clock<=1'b0;
    end

endmodule
