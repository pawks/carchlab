`include "RecursiveDoubleCarryAdder.v"
module CarryAdderTestbench();
    reg [15:0] InputA, InputB;
    wire [15:0] Output;
    reg CarryIn, Clock;
    wire CarryOut;
    Adder submodule1(.A(InputA), .B(InputB), .C(Output), .Overflow(CarryOut), .Cin(CarryIn), .clk(Clock));
    initial begin
    Clock<=1'b0;
    InputA<=16'habcd;
    InputB<=16'hbcde;
    CarryIn<=1'b0;
    #2;
    InputA<=16'h0006;
    InputB<=16'hc584;
    #2;
    InputA<=16'h063f;
    InputB<=16'h97ab;
    #2;
    InputA<=16'h1234;
    InputB<=16'h4321;
    #2;
    InputA<=16'h6789;
    InputB<=16'h9876;
    #2;
    InputA<=16'h0000;
    InputB<=16'h0000;
    #20;
    $finish;
    end
    always@(Output|InputA)
        $display("Time : %h Input : %h %h Output : %b %h Clock : %b",$time,InputA,InputB,CarryOut,Output,Clock);

    always begin
    #1 Clock<=1'b1;
    #1 Clock<=1'b0;
    end

endmodule
