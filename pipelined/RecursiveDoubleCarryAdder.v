module Adder(input [15:0] A, input [15:0] B, input Cin, output [15:0] C, output Overflow, input clk);
    wire [15:0] A,B,C;
    reg [15:0] g1,g2,g3,g4,p1,p2,p3,XorOut1,XorOut2,XorOut3,XorOut4;
    reg AndOut1,AndOut2,AndOut3,AndOut4;  //P for XOR And G for AND:
    wire [16:0] G,P; //C for Output
    wire Overflow, Cin;
    genvar i;
    assign P={A^B,1'b0};
    assign G={A&B,Cin};
    generate
    for(i=1;i<16;i=i+1) begin //s=naya g+ naya p*purana g 
        always @(posedge clk) begin
        g1[i]<=(G[i-1]&P[i])+G[i];
        p1[i]<=P[i]&P[i-1];
        AndOut1<=G[16];
        XorOut1<=P[16:1];
        end
    end
    for(i=2;i<16;i=i+1) begin //s=naya g+ naya p*purana g 
        always @(posedge clk) begin
        g2[i]<=(g1[i-2]&p1[i])+g1[i];
        p2[i]<=p1[i]&p1[i-2];
        AndOut2<=AndOut1;
        XorOut2<=XorOut1;
        end
    end
    for(i=4;i<16;i=i+1) begin //s=naya g+ naya p*purana g 
        always @(posedge clk) begin
        g3[i]<=(g2[i-4]&p2[i])+g2[i];
        p3[i]<=p2[i]&p2[i-4];
        AndOut3<=AndOut2;
        XorOut3<=XorOut2;
        end
    end
    for(i=8;i<16;i=i+1) begin //s=naya g+ naya p*purana g 
        always @(posedge clk) begin
        g4[i]<=(g3[i-8]&p3[i])+g3[i];
        AndOut4<=AndOut3;
        XorOut4<=XorOut3;
        end
    end

    always @(posedge clk) begin
    g1[0]<=G[0];

    g2[0]<=g1[0];
    g2[1]<=g1[1];
    end

    for(i=0;i<4;i=i+1)
        always @(posedge clk) g3[i]<=g2[i];

    for(i=0;i<8;i=i+1)
        always @(posedge clk) g4[i]<=g3[i]; 
    
    endgenerate
    assign C=XorOut4^g4;
    assign Overflow=(XorOut4[15]&g4[15])+AndOut4;
    //always@(P or G or p3 or g4)
    //    $display("PG %b %b %b %b",P,G,p3, g4);
endmodule
